import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './store/store';

import MainScreen from './screen/MainScreen';
import PatientsScreen from './screen/patients/PatientsScreen';
import PatientsEditScreen from './screen/patients/PatientsEditScreen';
import PriceScreen from './screen/price/PatientsScreen';
import StoreScreen from './screen/store/PatientsScreen';

import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <PersistGate
                persistor={persistor}
                loading={
                    <div>
                        ЗАГРУЗКА !!!!
                    </div>
                }>
                <Routes>
                    <Route path="/" element={<MainScreen/>}/>
                    <Route path="/patients" element={<PatientsScreen/>}/>
                    <Route path="/patients/edit" element={<PatientsEditScreen/>}/>
                    <Route path="/price" element={<PriceScreen/>}/>
                    <Route path="/store" element={<StoreScreen/>}/>
                </Routes>
            </PersistGate>
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
