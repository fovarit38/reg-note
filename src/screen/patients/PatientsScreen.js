import '../../assets/scss/app.scss';

import React, {useEffect, useState, useRef, forwardRef} from 'react';

import {CreatePatients, GetPatientOne, GetPatients, UpdatePatients} from '../../services';
import {connect} from 'react-redux';
import ActivityIndicator from 'react-activity-indicator';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link
} from "react-router-dom";

import State from "../../store/actions/StateActions";

import Layout from "../../components/layout/layout";
import Header from "../../components/header/header";
import HeaderModal from "../../components/header/headerModal";
import InputCustom from "../../components/input/input";
import InputMini from "../../components/input/inputMini";
import SelectCustom from "../../components/input/select";

import deleteIcon from '../../assets/img/delete.png';
import chevron from '../../assets/img/chevron-up-down.png';

// dots-horizontal.png
import dots from '../../assets/img/dots-horizontal.png';

import Modal from 'react-modal';
import eva_close from "../../assets/img/eva_close-fill.png";

function App({patients}) {

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [reload, setReload] = useState(false);
    const [sendForm, setSendForm] = useState(false);

    const [openMoreLoad, setOpenMoreLoad] = useState(false);

    const [genters, setGenders] = useState([
        {value: 'MALE', label: 'Муж'},
        {value: 'FEMALE', label: 'Жен'},
    ]);

    const [patientsEdit, setPatientsEdit] = useState(false);

    const [first_name, set_first_name] = React.useState("");
    const [last_name, set_last_name] = React.useState("");

    const [patronymic, set_patronymic] = React.useState("");
    const [gender, set_gender] = React.useState("");

    const [birth_date, set_birth_date] = React.useState("");
    const [iin, set_iin] = React.useState("");
    const [phone, set_phone] = React.useState("");


    async function getState() {
        State({
            patients: await GetPatients(),
        })
    }

    async function editPatient(id) {
        setIsOpen(true);
        setOpenMoreLoad(true);
        let patient = await GetPatientOne(id);
        setPatientsEdit(patient);
        set_first_name(patient?.first_name);
        set_last_name(patient?.last_name);
        set_iin(patient?.iin);
        set_phone(patient?.phone);
        set_birth_date(patient?.birth_date);
        set_patronymic(patient?.patronymic);
        set_gender(patient?.gender);
        setOpenMoreLoad(false);
    }

    async function createPatients() {
        if (!sendForm) {

            setSendForm(true);

            if (patientsEdit == false) {
                await CreatePatients({
                    first_name,
                    last_name,
                    patronymic,
                    iin,
                    phone,
                    birth_date,
                    gender
                });
            } else {
                await UpdatePatients(patientsEdit?.id, {
                    first_name,
                    last_name,
                    patronymic,
                    iin,
                    phone,
                    birth_date,
                    gender,
                });
            }

            clearModel();
            setIsOpen(false);
            setReload(!reload);
            setSendForm(false);
        }
    }

    function clearModel() {
        set_first_name("");
        set_last_name("");
        set_birth_date("");
        set_patronymic("");
        set_gender("");
        set_iin("");
        set_phone("");
    }

    function closeModal() {
        setIsOpen(false);
    }


    useEffect(() => {
        getState();
    }, [reload]);


    return (
        <Layout>

            <Modal
                className={"modalAdd"}
                isOpen={modalIsOpen}
                closeTimeoutMS={500}
                contentLabel="Example Modal"
            >
                {
                    openMoreLoad && (
                        <div className={"overplay"}>
                            <ActivityIndicator
                                number={5}
                                diameter={18}
                                borderWidth={1}
                                duration={200}
                                activeColor="#2CCBCB"
                                borderColor="#ccc"
                                borderWidth={2}
                                borderRadius="50%"
                            />
                        </div>
                    )
                }

                <HeaderModal close={closeModal}>
                    {patientsEdit == false && (
                        <p>Новый пациент</p>
                    )}
                    {patientsEdit != false && (
                        <p>Информация о пациенте</p>
                    )}
                </HeaderModal>

                <form className="inputs">

                    <InputCustom nameForm={"first_name"} value={first_name} handleChange={(text) => {
                        set_first_name(text.target.value)
                    }} name={"Имя"} placeholder={"Петр"}/>

                    <InputCustom nameForm={"last_name"} value={last_name} handleChange={(text) => {
                        set_last_name(text.target.value)
                    }} name={"Фамилия"} placeholder={"Первый "}/>

                    <InputCustom nameForm={"patronymic"} value={patronymic} handleChange={(text) => {
                        set_patronymic(text.target.value)
                    }} name={"Отчество"}
                                 placeholder={"Константинович"}/>

                    <InputCustom nameForm={"iin"} maxlength={12} handleChange={(text) => {
                        set_iin(text.target.value)
                    }} value={iin} name={"ИИН"} placeholder={"123456789012"}/>

                    <SelectCustom
                        handleChange={(text) => {
                            set_gender(text.value);
                        }}
                        value={genters.filter(option =>
                            option.value === gender)}
                        options={
                            genters
                        } name={"Пол"} placeholder={""}/>

                    <InputCustom type={"date"} nameForm={"birth_date"} handleChange={(text) => {
                        set_birth_date(text.target.value)
                    }} value={birth_date} name={"Дата рождения"} placeholder={"+7 (___) ___-__-__"}/>

                    <InputCustom nameForm={"phone"} handleChange={(text) => {
                        set_phone(text.target.value)
                    }} value={phone} name={"Телефон"} placeholder={"+7 (___) ___-__-__"}/>


                    <SelectCustom name={"Статус"} placeholder={""}/>
                    <InputCustom name={"Откуда пришел"} placeholder={"2gis"}/>
                </form>

                <div className="modal_footer">
                    <button onClick={closeModal}
                            className={'btn btn-cansel text text-16 text-bold text-color1'}>Отмена
                    </button>
                    <button onClick={createPatients}
                            className={'btn btn-on text text-16 text-bold text-color1'}>

                        {
                            !sendForm && "Сохранить"
                        }
                        {
                            sendForm && (
                                <ActivityIndicator
                                    number={5}
                                    diameter={15}
                                    borderWidth={1}
                                    duration={200}
                                    activeColor="#66D9EF"
                                    borderColor="white"
                                    borderWidth={2}
                                    borderRadius="50%"
                                />
                            )
                        }
                    </button>
                </div>
            </Modal>

            <Header>
                Пациенты
            </Header>
            <div className={'container-main container-main-table'}>
                <div className="container-main_table">
                    <div className="table-head">
                        <button className={"btn btn-add text text-s14"} onClick={() => {
                            clearModel();
                            setIsOpen(true);
                            setPatientsEdit(false);
                        }}>Новый пациент
                        </button>
                        <InputMini placeholder={"Карточка"}/>
                        <InputMini placeholder={"Имя и фамилия"}/>
                        <InputMini placeholder={"Телефон"}/>

                        <button className={"btn btn-remove text text-s14"} onClick={() => {
                        }}>Архив пациентов
                        </button>
                    </div>
                    <div className="table-box">
                        <table className="table-main">
                            <thead>
                            <tr>
                                <th className={'text text-s14'}></th>
                                <th className={'text text-color83 text-s14'}>
                                    <div className="row row-center">
                                        Карточка
                                        <img src={chevron} className={'icon icon-sort'} alt=""/>
                                    </div>
                                </th>
                                <th className={'text text-color83 text-s14'}>
                                    <div className="row row-center">
                                        Имя
                                        <img src={chevron} className={'icon icon-sort'} alt=""/>
                                    </div>
                                </th>
                                <th className={'text text-color83 text-s14'}>
                                    <div className="row row-center">
                                        Фамилия
                                        <img src={chevron} className={'icon icon-sort'} alt=""/>
                                    </div>
                                </th>
                                <th className={'text text-color83 text-s14'}>
                                    <div className="row row-center">
                                        Возраст
                                        <img src={chevron} className={'icon icon-sort'} alt=""/>
                                    </div>
                                </th>
                                <th className={'text text-color83 text-s14'}>
                                    <div className="row row-center">
                                        Телефон
                                        <img src={chevron} className={'icon icon-sort'} alt=""/>
                                    </div>
                                </th>
                                <th className={'text text-color83 text-s14'}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                patients && patients?.results && patients?.results.map((client) => {
                                    return (
                                        <tr>
                                            <td className={'text text-s14'}></td>
                                            <td className={'text text-s14'}>
                                                <NavLink activeClassName="active"
                                                         className={"nav-table"} to="/patients/edit">
                                                    {client?.card}
                                                </NavLink>
                                            </td>
                                            <td className={'text text-s14'}>
                                                <NavLink activeClassName="active"
                                                         className={"nav-table"} to="/patients/edit">
                                                    {client?.first_name}
                                                </NavLink>
                                            </td>
                                            <td className={'text text-s14'}>
                                                <NavLink activeClassName="active"
                                                         className={"nav-table"} to="/patients/edit">
                                                    {client?.last_name}
                                                </NavLink>
                                            </td>
                                            <td className={'text text-s14'}>
                                                <NavLink activeClassName="active"
                                                         className={"nav-table"} to="/patients/edit">
                                                    {client?.birth_date}
                                                </NavLink>
                                            </td>
                                            <td className={'text text-s14'}>
                                                <NavLink activeClassName="active"
                                                         className={"nav-table"} to="/patients/edit">
                                                    {client?.phone}
                                                </NavLink>
                                            </td>
                                            <td className={'text text-s14'}>
                                                <img src={deleteIcon} className={'icon icon-delete'} alt=""/>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </Layout>
    );
}


const mapStateToProps = state => ({
    patients: state.state.patients,
});

export default connect(mapStateToProps)(App);
