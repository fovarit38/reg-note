import '../../assets/scss/app.scss';

import React, {useEffect, useState, useRef, forwardRef} from 'react';

import {CreatePatients, GetPatientOne, GetPatients, UpdatePatients} from '../../services';
import {connect} from 'react-redux';
import ActivityIndicator from 'react-activity-indicator';

import State from "../../store/actions/StateActions";

import Layout from "../../components/layout/layout";
import Header from "../../components/header/header";

import avatar from "../../assets/img/icon/avatar.png";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link
} from "react-router-dom";
import chevron from "../../assets/img/chevron-up-down.png";
import edit from "../../assets/img/icon/Edit.png";
import HeaderModal from "../../components/header/headerModal";
import InputCustom from "../../components/input/input";
import SelectCustom from "../../components/input/select";


function App({patients}) {

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [reload, setReload] = useState(false);
    const [sendForm, setSendForm] = useState(false);

    const [openMoreLoad, setOpenMoreLoad] = useState(false);

    const [genters, setGenders] = useState([
        {value: 'MALE', label: 'Муж'},
        {value: 'FEMALE', label: 'Жен'},
    ]);

    const [patientsEdit, setPatientsEdit] = useState(false);

    const [first_name, set_first_name] = React.useState("");
    const [last_name, set_last_name] = React.useState("");

    const [patronymic, set_patronymic] = React.useState("");
    const [gender, set_gender] = React.useState("");

    const [birth_date, set_birth_date] = React.useState("");
    const [iin, set_iin] = React.useState("");
    const [phone, set_phone] = React.useState("");


    async function getState() {
        State({
            patients: await GetPatients(),
        })
    }

    async function editPatient(id) {
        setIsOpen(true);
        setOpenMoreLoad(true);
        let patient = await GetPatientOne(id);
        setPatientsEdit(patient);
        set_first_name(patient?.first_name);
        set_last_name(patient?.last_name);
        set_iin(patient?.iin);
        set_phone(patient?.phone);
        set_birth_date(patient?.birth_date);
        set_patronymic(patient?.patronymic);
        set_gender(patient?.gender);
        setOpenMoreLoad(false);
    }

    async function createPatients() {
        if (!sendForm) {

            setSendForm(true);

            if (patientsEdit == false) {
                await CreatePatients({
                    first_name,
                    last_name,
                    patronymic,
                    iin,
                    phone,
                    birth_date,
                    gender
                });
            } else {
                await UpdatePatients(patientsEdit?.id, {
                    first_name,
                    last_name,
                    patronymic,
                    iin,
                    phone,
                    birth_date,
                    gender,
                });
            }

            clearModel();
            setIsOpen(false);
            setReload(!reload);
            setSendForm(false);
        }
    }

    function clearModel() {
        set_first_name("");
        set_last_name("");
        set_birth_date("");
        set_patronymic("");
        set_gender("");
        set_iin("");
        set_phone("");
    }

    function closeModal() {
        setIsOpen(false);
    }


    useEffect(() => {
        getState();
    }, [reload]);


    return (
        <Layout>
            <Header>
                Пациенты
            </Header>
            <div className="container-main container-edit">
                <div className="profile_box">
                    <div className="profile">
                        <div className="profile_img">
                            <img src={avatar} className={'icon icon-avatar'} alt=""/>
                        </div>
                        <div className="profile_name">
                            <p className="text text-bold text-s20">
                                Ксения Павлова Константиновна
                            </p>
                            <p style={{color: "#219653"}} className="text text-bold text-s14">
                                Постоянный
                            </p>
                            <p className="text text-color1 text-bold text-s14">
                                +7 (777) 123-45-67
                            </p>
                        </div>
                    </div>
                    <div className="profile_box_price">
                        <p className="profile_box_right text text-bold text-color1 text-s16">
                            Долг:
                        </p>
                        <p style={{color:"#EB5757"}} className="text text-bold text-s16">
                            5 000 000 ₸
                        </p>
                    </div>
                </div>
                <div className="profile-btns">
                    <button className={"btn btn-in text text-s14"} onClick={() => {

                    }}>
                        <img src={edit} className={'icon icon-edit'} alt=""/>
                        Новый визит
                    </button>
                </div>
                <div className="profile-info">
                    <div className="profile-info_nav">
                        <button className={"btn btn-nav active text text-s14"} onClick={() => {

                        }}>
                            Информация
                        </button>
                        <button className={"btn btn-nav text text-s14"} onClick={() => {

                        }}>
                            Счета
                        </button>

                        <button className={"btn btn-nav text text-s14"} onClick={() => {

                        }}>
                            История посещений
                        </button>

                        <button className={"btn btn-nav text text-s14"} onClick={() => {

                        }}>
                            Файлы
                        </button>
                    </div>
                    <div className="profile-info_main">
                        <div className="profile-info_main_edit">
                            <p className={'text text-bold text-color1 text-s24'}>Информация о пациенте</p>

                            <form className="inputs">

                                <InputCustom nameForm={"first_name"} value={first_name} handleChange={(text) => {
                                    set_first_name(text.target.value)
                                }} name={"Имя"} placeholder={"Петр"}/>

                                <InputCustom nameForm={"last_name"} value={last_name} handleChange={(text) => {
                                    set_last_name(text.target.value)
                                }} name={"Фамилия"} placeholder={"Первый "}/>

                                <InputCustom nameForm={"patronymic"} value={patronymic} handleChange={(text) => {
                                    set_patronymic(text.target.value)
                                }} name={"Отчество"}
                                             placeholder={"Константинович"}/>

                                <InputCustom nameForm={"iin"} maxlength={12} handleChange={(text) => {
                                    set_iin(text.target.value)
                                }} value={iin} name={"ИИН"} placeholder={"123456789012"}/>

                                <SelectCustom
                                    handleChange={(text) => {
                                        set_gender(text.value);
                                    }}
                                    value={genters.filter(option =>
                                        option.value === gender)}
                                    options={
                                        genters
                                    } name={"Пол"} placeholder={""}/>

                                <InputCustom type={"date"} nameForm={"birth_date"} handleChange={(text) => {
                                    set_birth_date(text.target.value)
                                }} value={birth_date} name={"Дата рождения"} placeholder={"+7 (___) ___-__-__"}/>

                                <InputCustom nameForm={"phone"} handleChange={(text) => {
                                    set_phone(text.target.value)
                                }} value={phone} name={"Телефон"} placeholder={"+7 (___) ___-__-__"}/>


                                <SelectCustom name={"Статус"} placeholder={""}/>
                                <InputCustom name={"Откуда пришел"} placeholder={"2gis"}/>
                            </form>

                            <button className={"btn btn-in btn-in-mini text text-s14"} onClick={() => {

                            }}>
                                <img src={edit} className={'icon icon-edit'} alt=""/>
                                Редактировать
                            </button>
                        </div>
                        <div className="profile-info_main_fite">
                            <p className="text notes-title text-bold text-s20">
                                Заметки
                            </p>
                            <div className="notes">
                                <div className="notes_name text notes-title text-color1 text-s14">
                                    Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit
                                    officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud
                                    amet.Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit
                                    officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud
                                    amet.
                                </div>
                                <div className="notes_btn">
                                    <p style={{color: "#2CCBCB"}} className="text notes-title text-bold text-s14">
                                        Редактировать
                                    </p>
                                    <p style={{color: "#EB5757"}} className="text notes-title text-bold text-s14">
                                        Удалить
                                    </p>
                                </div>
                            </div>
                            <div className="notes_message">
                                <p className="text notes-mtitle text-bold text-color1 text-s14">
                                    Новая заметка
                                </p>
                                <textarea placeholder="Введите текст"
                                          className="notes_input text text-color1 text-s14"></textarea>
                                <div className="notes_message_btns">
                                    <div className="text text-s14">
                                        Прикрепить файл
                                    </div>
                                    <button className={"btn btn-add btn-add-mini text text-s14"} onClick={() => {
                                    }}>
                                        Добавить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Layout>
    );
}


const mapStateToProps = state => ({
    patients: state.state.patients,
});

export default connect(mapStateToProps)(App);
