import React from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link
} from "react-router-dom";
import logo from '../../assets/img/logo.png';


const Layout = ({children}) => {
    return (
        <>
            <div className={'main-box'}>
                <div className={'siteBar'}>
                    <img src={logo} className={'logo'} alt=""/>
                    <nav>
                        <NavLink activeClassName="active" className={"nav-link nav-link-calendar text text-s16"} to="/">
                            Календарь
                        </NavLink>
                        <NavLink activeClassName="active" className={"nav-link nav-link-patients text text-s16"}
                                 to="/patients">
                            Пациенты
                        </NavLink>
                        <NavLink activeClassName="active" className={"nav-link nav-link-price text text-s16"}
                                 to="/price">
                            Цены и услуги
                        </NavLink>
                        <NavLink activeClassName="active" className={"nav-link nav-link-store text text-s16"}
                                 to="/store">
                            Склад
                        </NavLink>
                    </nav>

                </div>
                <main>{children}</main>
            </div>
        </>
    )
}
export default Layout;