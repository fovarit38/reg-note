import React from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link
} from "react-router-dom";
//
const Header = ({children}) => {
    return (
        <>
            <div className={'container-main container-main-header'}>
                <div className={'main-box'}>
                    <p className={'text text-bold text-color1 text-s24'}>{children}</p>
                </div>
            </div>
        </>
    )
}
export default Header;