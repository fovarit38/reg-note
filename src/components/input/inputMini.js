import React from 'react';

const Button = ({name, placeholder, type = "text", maxlength = 255, nameForm, value, handleChange}) => {
    return (
        <>
            <div className={'input input-mini'}>
                <input name={nameForm}  onChange={handleChange} value={value}
                       placeholder={placeholder}
                       className={'input_in text text-s14'}
                       type={type}/>
            </div>
        </>
    )
}
export default Button;