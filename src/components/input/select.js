import React from 'react';
import Select from 'react-select'

const Button = ({name, placeholder,value,handleChange, options = []}) => {
    return (
        <>
            <Select value={value} onChange={handleChange} className={'select select-def text text-s16'} placeholder={<div>{name}</div>}
                    options={options}/>
        </>
    )
}
export default Button;